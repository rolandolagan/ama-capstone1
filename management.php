<?php
    require "inc/template.php";

    function title(){
        echo "O & E Pharma Management Page";
    }

    function content(){
?>
    <div class="main-container">
        <div class="sidebar">
            <div class="sidebar__company-name">
                <h2 class="sidebar__company-headname">O & E Pharmacy</h2>
                <hr>
                <h2 class="sidebar__company-tabname">Management</h2>
            </div>

            <div class="sidebar__user-info">
                <img class="sidebar__user-image" src="assets/images/icons/profile.png" alt="">
                <h3>Welcome, Admin</h3>
            </div>

            <nav class="">
                <ul class="">
                    <li class="">
                        <a href="" class=""></a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
<?php
    }
?>