<?php
    require "inc/template.php";

    function title(){
        echo "O & E Pharma Main Page";
    }

    function content(){
?>
    <div class="main-container">
        <nav class="nav">

        </nav>

        <main class="main-menu">
            <a href="management.php" class="menu">
                <img src="assets/images/icons/management.png" alt="" class="menu__icon">
                <h3 class="menu__caption">Management</h3>
            </a>

            <a href="#" class="menu">
                <img src="assets/images/icons/cashier.png" alt="" class="menu__icon">
                <h3 class="menu__caption">Cashier</h3>
            </a>

            <a href="#" class="menu">
                <img src="assets/images/icons/report.png" alt="" class="menu__icon">
                <h3 class="menu__caption">Report</h3>
            </a>
        </main>
    </div>
<?php
    }
?>