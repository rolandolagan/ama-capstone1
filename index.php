<?php
    require "inc/template.php";

    function title(){
        echo "O & E Pharma Login Page";
    }

    function content(){
?>
    <div class="login-container">
        <div class="logo">
            <div class="logo__company-name">
                <span>O & E</span>
                <span>Pharmacy</span>
            </div>
            <img src="assets/images/logos/o&e-logo.png" alt="" class="logo__company-logo">
        </div>

        <div class="login">
            <form action="main.php" class="login__form">
                <div class="login__form-group">
                    <input class="login__field" type="text"  placeholder="Email">
                    <svg class="login__icon">
                        <use xlink:href="assets/images/icons/sprite.svg#icon-mail"></use>
                    </svg>
                </div>
                <div class="login__form-group">
                    <input class="login__field" type="password"  placeholder="Password">
                    <svg class="login__icon">
                        <use xlink:href="assets/images/icons/sprite.svg#icon-lock"></use>
                    </svg>
                </div>
                <button class="login__button" type="submit">Login</button>
            </form>
        </div>

        <div class="employee">
            <form action="" class="employee__form">
                <div class="employee__form-group">
                    <input class="employee__field" type="text"  placeholder="Employee ID" maxlength="25">
                </div>
                <button class="employee__button" type="submit">Time-In</button>
            </form>
        </div>
    </div>
<?php
    }
?>